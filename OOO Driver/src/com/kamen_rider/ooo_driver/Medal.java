/**
 * 
 */
package com.kamen_rider.ooo_driver;

import android.graphics.Bitmap;

/**
 * @author Abba
 *
 */
public class Medal
	extends Sprite
{
	public int place = 0;
	
	public Medal()
	{
		super();
	}
	
	@Override
	public void init(Bitmap bitmap)
	{
		super.init(bitmap);
		autoxs = width / 100;
		autoys = height / 100;
	}
	
	public Vector getPlace(int n, Vector v)
	{
		Vector res = new Vector();
		res.x = v.x;
		int buffer = 5;
		if (0 <= n && n < 4)
			res.y = v.y - (n * height) - ((n + 1) * buffer);
		else if (n == 4)
			res.y = v.y + 2 * height + 2 * buffer;
		else
			res.y = v.y + height + buffer;
		res.y -= height / 2;
		return res;
	}
	
	public void moveToPlace(int nPlace, Vector v)
	{
		nPlace %= 6;
		if (nPlace < 0)
			nPlace += 6;
		pos = getPlace(nPlace, v);
		place = nPlace;
	}
	
	/**
	 * Animated movement to place and set place
	 * @param n Place New place
	 * @param v Alignment vector
	 */
	public void animToPlace(int nPlace, Vector v)
	{
		place = animPlace(nPlace, v);
	}
	
	/**
	 * Animated movement to place
	 * @param nPlace New place
	 * @param v Alignment vector
	 */
	public int animPlace(int nPlace, Vector v)
	{
		nPlace %= 6;
		if (nPlace < 0)
			nPlace += 6;
		Vector nPos = getPlace(nPlace, v);
		if (nPlace == 3 || (nPlace == 4 && place != 5))
		{
			pos = nPos;
			dest = Vector.Zero();
		}
		else
		{
			nPos.x -= width / 2;
			nPos.y -= height / 2;
			dest = nPos;
		}
		return nPlace;
	}
	
	public float getRadius()
	{
		return Math.min(height, width) / 2;
	}
	
	@Override
	public boolean inside(Vector mouse)
	{
		if (Vector.sub(getCenter(), mouse).length() <= getRadius()) return true;
		return false;
	}
	
	public void update()
	{
		super.update();
	}
	
	
	/**
	 * Centered
	 */
	public void drag(Vector mouse, Vector center, boolean useX, boolean useY)
	{
		Vector nPos = center.clone();
		nPos.x -= width / 2;
		nPos.y -= height / 2;
		if (useX)
		{
			nPos.x = Math.min(nPos.x, mouse.x - (width / 2));
		}
		if  (useY)
		{
			nPos.y = Math.min(nPos.y, mouse.y - (height / 2));
		}
		this.pos = nPos;
	}
}
