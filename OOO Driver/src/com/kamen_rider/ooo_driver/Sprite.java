package com.kamen_rider.ooo_driver;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Basic Sprite
 * Need to support animated movement through matrix and alpha
 * @author Arthur C
 * 
 */
public class Sprite {
	protected Bitmap image;
	protected Vector pos;
	
	protected int alpha = 255;
	protected float xScale = 1.0f;
	protected float yScale = 1.0f;
	protected int fadeMode = 0;
	protected int frames = 30;
	protected Vector pivot; 
	protected float angle = 0;
	/**
	 *	Destination vector - center point
	 */
	protected Vector dest;
	
	public float height;
	public float width;
	public float autoxs;
	public float autoys;
	
	public Sprite()
	{
		pos = new Vector();
		dest = Vector.Zero();
	}
	
	public void update()
	{
		if (dest == null)
			dest = getPos();
		
		if (fadeMode == 1)
		{
			alpha += 255 / frames;
			if (alpha >= 255)
			{
				alpha = 255;
				fadeMode = 0;
			}
		}
		if (fadeMode == 2)
		{
			alpha -= 255 / frames;
			if (alpha <= 0)
			{
				alpha = 0;
				fadeMode = 0;
			}
		}
		
		if (!dest.zero())
		{
			float dX = (this.dest.x - this.pos.x + width / 2) / frames;
			float dY = (this.dest.y - this.pos.y + height / 2) / frames;
			if (Math.abs(dX) > 0.01f || Math.abs(dY) > 0.01f)
			{
				this.pos.x += dX;
				this.pos.y += dY;
			}
			else
				dest = Vector.Zero();
		}
	}

	public void init(Bitmap bitmap)
	{
		image = bitmap;
		height = bitmap.getHeight();
		width = bitmap.getWidth();
		
		xScale = 1.0f;
		yScale = 1.0f;
		
		pivot = getCenter();
	}
	
	public Matrix getMatrix()
	{
		Matrix m = new Matrix();
		m.setTranslate(pos.x, pos.y);
		m.postScale(xScale, yScale, getCenter().x, getCenter().y);
		m.postRotate(angle, pivot.x, pivot.y);
		//TODO: Rotations, scaling
		return m;
	}
	
	public Paint getPaint()
	{
		Paint paint = new Paint();
		paint.setAlpha(alpha);
		paint.setAntiAlias(true);
		
		return paint;
	}
	
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(image, getMatrix(), getPaint());
	}
	
	public RectF getBounds()
	{
		RectF bounds = new RectF(0, 0, width, height);
		getMatrix().mapRect(bounds);
		return bounds;
	}
	
	public boolean inside(Vector mouse)
	{
		if (Math.floor(angle) == 0)
		{
			RectF bounds = getBounds();
			if (mouse.x < bounds.left || mouse.x > bounds.right) return false;
			if (mouse.y < bounds.top || mouse.y > bounds.bottom) return false;
			return true;
		}
		else
			return inside2(mouse);
	}
	
	public void drag(Vector mouse, Vector center, boolean useX, boolean useY)
	{
		if (center == null)
			center = Vector.Zero();
		Vector nPos = center.clone();
		if (useX)
		{
			nPos.x = Math.min(center.x, mouse.x - (width / 2));
		}
		if  (useY)
		{
			nPos.y = Math.min(center.y, mouse.y - (height / 2));
		}
		this.pos = nPos;
	}
	
	public Vector getPos()
	{
		return pos.clone();
	}
	
	/**
	 * Sets xy
	 * @param v Position on the plane
	 */
	public void setPos(Vector v)
	{
		pos = v.clone();
	}
	
	public Vector getCenter()
	{
		return Vector.create(pos.x + width / 2, pos.y + height / 2);
	}
	
	/**
	 * Set the center of the sprite
	 * @param center A position on the plane
	 */
	public void setCenter(Vector center)
	{
		pos = center.clone();
		pos.x -= width / 2;
		pos.y -= height / 2;
	}
	
	public void setCenter(int i, int j) {
		setCenter(Vector.create(i, j));
	}
	
	public void rotate(float angle, Vector origin)
	{
		this.angle = angle;
		this.pivot = origin.clone();
	}
	
	public void fadeIn()
	{
		fadeMode = 1;
	}
	
	public void fadeOut()
	{
		fadeMode = 2;
	}
	
	private Vector rotate(Vector point)
	{
		Vector r = new Vector();
		float rad = (float) (angle * (Math.PI / 180));
		r.x = (float) (pivot.x + Math.cos(rad) * (point.x - pivot.x) - Math.sin(rad) * (point.y - pivot.y));
		r.y = (float) (pivot.y + Math.sin(rad) * (point.x - pivot.x) + Math.cos(rad) * (point.y - pivot.y));
		return r;
	}
	
	public boolean inside2(Vector mouse)
	{
		Vector ur = getPos();
		Vector ul = Vector.create(pos.x + width, pos.y);
		Vector br = Vector.create(pos.x, pos.y + height);
		Vector bl = Vector.create(pos.x + width, pos.y + height);
		ur = rotate(ur);
		ul = rotate(ul);
		br = rotate(br);
		bl = rotate(bl);
		Vector axis = Vector.sub(ur, ul);
		Vector pM = Vector.proj(mouse, axis);
		Vector b1 = Vector.proj(ur, axis);
		Vector b2 = Vector.proj(ul, axis);
		if (pM.x < b1.x || pM.x > b2.x) return false;
		axis = Vector.sub(ul, bl);
		pM = Vector.proj(mouse, axis);
		b1 = Vector.proj(ul, axis);
		b2 = Vector.proj(bl, axis);
		if (pM.y < b1.y || pM.y > b2.y) return false;
		return true;
	}
}
