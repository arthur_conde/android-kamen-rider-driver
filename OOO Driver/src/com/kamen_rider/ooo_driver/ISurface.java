package com.kamen_rider.ooo_driver;

import android.graphics.Canvas;

public interface ISurface {
	void onInitialize();
	void onDraw(Canvas canvas);
	void onUpdate(long gameTime);
}
