package com.kamen_rider.ooo_driver;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

public class henshinSound implements Runnable {
	private SoundPool mSoundPool;
	private HashMap<Integer, Integer> mSoundPoolMap;
	private HashMap<Integer, Integer> duraMap;
	private AudioManager  mAudioManager;
	private Context mContext;
	private Thread soundThread;
	public ArrayList<Holder> queue;
 
	public henshinSound(Context theContext)
	{
		mContext = theContext;
	    mSoundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
	    mSoundPoolMap = new HashMap<Integer, Integer>();
	    duraMap = new HashMap<Integer, Integer>();
	    mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
	    soundThread = new Thread(this);
	    queue = new ArrayList<Holder>();
	    soundThread.start();
	} 
 
	/**
	 * Add a new Sound to the SoundPool
	 *
	 * @param Index - The Sound Index for Retrieval
	 * @param SoundID - The Android ID for the Sound asset.
	 */
	public void loadSound(int Index,int SoundID)
	{
		mSoundPoolMap.put(Index, mSoundPool.load(mContext, SoundID, 1));
	}
 
	/**
	 * Loads the various sound assets
	 * Stores duration in hashmap
	 * 
	 * @param id - The Resource ID for the sound
	 */
	public int loadSound2(int id)
	{
		int len = mSoundPoolMap.size();
		mSoundPoolMap.put(len, mSoundPool.load(mContext, id, 1));
		MediaPlayer m = MediaPlayer.create(mContext, id);
		if (m == null)
		{
			duraMap.put(len, 800);
			Log.d("loadSound2", "Failed to load: " + id);
		}
		else
		{
			duraMap.put(len, m.getDuration());
			m.release();
		}
		return len;
	}
	
	/**
	 * Loads the various sound assets
	 * Currently hard coded but could easily be changed to be flexible.
	 * 
	 * @param id - The Resource ID for the sound
	 */
	public int loadSound(int id)
	{
		int len = mSoundPoolMap.size();
		mSoundPoolMap.put(len, mSoundPool.load(mContext, id, 1));
		return len;
	}
 
	/**
	 * Plays a Sound
	 *
	 * @param index - The Index of the Sound to be played
	 * @param speed - The Speed to play not, not currently used but included for compatibility
	 */
	public void playSound(int index,float speed)
	{
		     float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		     streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		     mSoundPool.play(mSoundPoolMap.get(index), streamVolume, streamVolume, 1, 0, speed);
	}
	
	/**
	 * Plays a Sound
	 *
	 * @param index - The Index of the Sound to be played
	 */
	public void playSound(int index)
	{
		     float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		     streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		     mSoundPool.play(mSoundPoolMap.get(index), streamVolume, streamVolume, 1, 0, 1.0f);
	}
	
	 /** Plays a Sound
	 *
	 * @param index - The Index of the Sound to be played
	 */
	public void qSound(int index, int ms)
	{
		queue.add(new Holder(index, ms));
	}
 
	/**
	 * Stop a Sound
	 * @param index - index of the sound to be stopped
	 */
	public void stopSound(int index)
	{
		mSoundPool.stop(mSoundPoolMap.get(index));
	}
 
	/**
	 * Deallocates the resources and Instance of SoundManager
	 */
	public void cleanup()
	{
		mSoundPool.release();
		mSoundPool = null;
	    mSoundPoolMap.clear();
	    mAudioManager.unloadSoundEffects();
 
	}

	@Override
	public void run() {
		while(true)
		{
			if (queue.size() == 0)
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					
				}
				continue;
			}
			Holder h = queue.remove(0);
			playSound(h.id);
			if (h.ms != 0)
			{
				float dura = duraMap.get(h.id);
				try {
					Thread.sleep((long) (dura * (3f / 5f)));
				} catch (InterruptedException e) {
					
				}
			}
		}
	}
	
	public class Holder
	{
		public int id;
		public int ms = 0;
		
		public Holder(int id, int ms)
		{
			this.id = id;
			this.ms = ms;
		}
	}

	public void qSound(int i) {
		queue.add(new Holder(i, 0));
	}
 
}
