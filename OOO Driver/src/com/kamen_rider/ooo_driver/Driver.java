package com.kamen_rider.ooo_driver;

import android.graphics.Bitmap;

public class Driver
	extends Sprite
{
	public TransformEvent onTransform;
	public Driver()
	{
		super();
	}
	
	public void init(Bitmap bitmap)
	{
		super.init(bitmap);
		
		autoys = height / 177;
		autoxs = width / 458;
		
	}
	
	public void setXform(TransformEvent event)
	{
		onTransform = event;
	}
	
	public void rotate(float angle, Vector origin)
	{
		if (Math.floor(angle) == 0)
		{
			angle = 0;
			if (onTransform != null)
				onTransform.endTransform();
		}
		if ((angle >= 20 && Math.signum(angle) > 0)|| (angle >= -340 && Math.signum(angle) < 0))
		{
			if (onTransform != null)
				onTransform.transform();
		}
		this.angle = angle;
		this.pivot = origin.clone();
	}
}
