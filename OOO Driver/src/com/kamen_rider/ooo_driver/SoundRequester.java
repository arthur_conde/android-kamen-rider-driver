package com.kamen_rider.ooo_driver;

public interface SoundRequester {
	public void playSound(int id);
	public void playSound(int id, int d);
	
	public void playScanner(int scanner);
	
	public void playCombo(int color);
	
	public void playTaToBa();
	/**
	 * Play the voice overs.
	 * Colors are 0 = Green, 1 = Yellow, 2 = Gray/Silver, 3 = Red, 4 = Blue, 5 = Pink  
	 * @author Abba 
	 * @param medalId Array holding the colors
	 */
	public void playVO(int[] medalId);
	
	public void playExtra(int i);
	
	public void playScanner(int s, int delay);
}
