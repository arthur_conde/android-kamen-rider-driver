package com.kamen_rider.ooo_driver;

public interface TransformEvent {
	public void transform();
	public void endTransform();
}
