package com.kamen_rider.ooo_driver;

/**
 * Holds a coordinate in the x-y plane
 * @author Abba
 *
 */
public class Vector
{
	/**
	 * X-Coordinate
	 */
	public float x;
	/**
	 * Y-Coordinate
	 */
	public float y;
	
	public Vector()
	{
		x = y = 0;
	}
	
	public Vector clone()
	{
		return Vector.create(x,y);
	}
	
	public static Vector create()
	{
		return new Vector();
	}
	
	public static Vector create(float x, float y)
	{
		Vector v = new Vector();
		v.x = x;
		v.y = y;
		return v;
	}
	
	/**
	 * Subtracts B from A
	 * @param A
	 * @param B
	 * @return The result of B - A or the line AB
	 */
	public static Vector sub(Vector A, Vector B)
	{
		return Vector.create(B.x - A.x, B.y - A.y);
	}
	
	public static Vector add(Vector A, Vector B)
	{
		return Vector.create(A.x + B.x, A.y + B.y);
	}
	
	public double Angle(Vector A, Vector B)
	{
		Vector L1 = Vector.sub(this, A);
		Vector L2 = Vector.sub(this, B);
		double atan2 = Math.atan2(L2.y, L2.x) - Math.atan2(L1.y, L1.x);
		return atan2;
	}
	
	public static double length(Vector v)
	{
		return Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2));
	}
	
	public static double sqlen(Vector v)
	{
		return Math.pow(v.x, 2) + Math.pow(v.y, 2);
	}
	
	public double length()
	{
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	public boolean zero()
	{
		if (this.x == 0 && this.y == 0)
			return true;
		return false;
	}
	
	public static boolean zero(Vector v)
	{
		if (v.x == 0 && v.y == 0) return true;
		return false;
	}
	
	/**
	 * Create a [0, 0] vector
	 * @return A zero vector
	 */
	public static Vector Zero()
	{
		return Vector.create(0f, 0f);
	}
	
	@Override
	public String toString()
	{
		return "[" + x + ", " + y + "]";
	}
	
	public boolean varEquals(Vector a, float error)
	{
		if (Math.abs((this.x - a.x) / a.x) > error)
			return false;
		if (Math.abs((this.y - a.y) / a.y) > error)
			return false;
		return true;
	}
	
	public float[] getArray()
	{
		float[] r = new float[2];
		r[0] = x;
		r[1] = y;
		return r;
	}
	
	public static Vector[] fromArray(float[] v)
	{
		Vector[] r = new Vector[v.length / 2];
		int j = 0;
		for(int i = 0; i < v.length; i+= 2)
		{
			r[j++] = create(v[i], v[i + 1]);
		}
		return r;
	}
	
	public static float[] getArray(Vector[] v)
	{
		float[] r = new float[v.length * 2];
		int j = 0;
		for(int i = 0; i < v.length; i++)
		{
			r[j++] = v[i].x;
			r[j++] = v[i].y;
		}
		return r;
	}
	
	public static float dot(Vector A, Vector B)
	{
		return A.x * B.x + A.y * B.y;
	}
	
	public static Vector proj(Vector point, Vector Axis)
	{
		Vector r = new Vector();
		r.x = (float) (Axis.x * (Vector.dot(point, Axis) / Vector.sqlen(Axis)));
		r.y = (float) (Axis.y * (Vector.dot(point, Axis) / Vector.sqlen(Axis)));
		return r;
	}
}

