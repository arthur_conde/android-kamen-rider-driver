package com.kamen_rider.ooo_driver;

import android.graphics.Bitmap;

public class OOOScanner
	extends Sprite
{
	public boolean scanning = false;
	public void init(Bitmap bitmap)
	{
		super.init(bitmap);
		this.pivot = getCenter();
	}
	
	public void drag(Vector mouse)
	{
		this.pos.x = mouse.x - width / 2;
		this.pos.y = mouse.y - height / 2;
		pivot = getCenter();
	}
	
	public void scan(Vector mouse, Vector begin, Vector end)
	{
		scanning = true;
		Vector line = Vector.sub(begin, end);
		float slope = line.y / line.x;
		this.pos.x = mouse.x - width / 2;
		this.pos.y = getY(slope, mouse.x, begin) - height / 2;
		pivot = getCenter();
	}
	
	public float getY(float slope, float x, Vector pt)
	{
		return (slope * (x - pt.x)) + pt.y;
	}
	
	public void stopScan()
	{
		scanning = false;
	}
}
