package com.kamen_rider.ooo_driver;

import java.lang.reflect.Field;
import java.util.HashMap;

import android.content.Context;

public class ResFactory {
	private static HashMap<String, Integer> m_hash;
	
	public static void init(Context context)
	{
		m_hash = new HashMap<String, Integer>();
		Field[] fields = R.drawable.class.getFields();
		for(Field field : fields)
		{
			try
			{
				//Log.d("ResFactory", field.getName() + ":" + field.getInt(null));
				m_hash.put(field.getName(), field.getInt(null));
			}
			catch(Exception e)
			{
				
			}
		}
	}
	
	public static int get(String name)
	{
		if (m_hash.containsKey(name))
			return m_hash.get(name);
		return 0;
	}
}
