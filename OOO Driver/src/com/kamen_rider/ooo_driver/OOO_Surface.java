package com.kamen_rider.ooo_driver;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

public class OOO_Surface
	extends DrawablePanel
	implements TransformEvent, EmblemEvents
{
	private Driver driver;
	private OOOScanner scanner;
	private Driver[] dSlot;
	private Medal[][] medals;
	private Emblem[] emblems;
	private int[] medalId;
	private boolean fDraw = false;
	private SoundRequester sMgr;
	
	private Vector[] vPlaces;
	private Vector[] vLanes;
	
	private boolean dragDriver = false;
	
	/**
	 * Set when a medal is clicked
	 */
	private boolean[] clickMedal = {false, false, false};
	/**
	 * Set when a medal is picked
	 */
	private boolean[] setMedal = {false, false, false};
	/**
	 * Set to allow a medal to be dragged
	 */
	private boolean[] dragMedal = {false, false, false};
	/**
	 * Set when a medal is finalized
	 */
	private boolean[] finMedal = {false, false, false};
	/**
	 * Set when a medal is scanned
	 */
	private boolean[] scanMedal = {false, false, false};
	/**
	 * Set when a medal is in position
	 */
	private boolean[] inPosition = {false, false, false};
	/**
	 * Set when a medal is scanned for scanning charge
	 */
	private boolean[] chargeMedal = {false, false, false};
	
	private int[] rRad = {0, 0, 0};
	private int[] rAlpha = {255, 255, 255};
	private int[] rRed = {0x2f, 228, 113, 202, 55, 0xb8};
	private int[] rGreen = {0x6d, 178, 122, 45, 91, 0x7b};
	private int[] rBlue = {0x1e, 43, 113, 35, 211, 0x9c};
	private boolean[] rLinger = {false, false, false};
	private boolean[] rFull = {false, false, false};
	
	/**
	 * Set when voice over is played
	 */
	private boolean pVO = false;
	/**
	 * Set when scan wait is played
	 */
	private boolean pScan = false;
	/**
	 * Set when scan start is played
	 */
	private boolean pScanStart = false;
	
	/**
	 * Set when transformed
	 */
	private boolean isXform = false;
	/**
	 * Set when transform is requested
	 */
	private boolean reqXform = false;
	/**
	 * Set when untransform is requested
	 */
	private boolean reqUnxform = false;
	/**
	 * Set to prevent changing driver angle
	 */
	private boolean lckDriver = false;
	/**
	 * Set when untransform sound is played
	 */
	private boolean pUnxform = false;
	/**
	 * Set when the driver is in the closed position. Angle = 0
	 */
	private boolean driverClosed = true;
	private boolean canScan = false;
	/**
	 * Set to show the scanner
	 */
	private boolean showScanner = false;
	/**
	 * Set to begin counting down to hide the combo pic
	 */
	private boolean hideEmblem = false;
	/**
	 * Set to allow scanning charge
	 */
	private boolean canScanCharge = false;
	/**
	 * Set when using scan charge
	 */
	private boolean isCharge = false;
	
	private int shownEmblems = 0;
	private int ticks = 0;
	private int maxTick = 150;
	
	public OOO_Surface(Context context) {
		super(context);
		sMgr = (SoundRequester) context;
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		canvas.drawColor(Color.BLACK);
		
		if (!fDraw && medals[2][5] != null)
		{
			driver.setCenter(getWidth() / 2, getHeight() / 2); //5 * 3
			scanner.setCenter(Vector.create(0,0));
			scanner.angle = 110;
			
			vPlaces[0] = Vector.create(driver.pos.x + 62 * driver.autoxs, driver.pos.y + 94 * driver.autoys);
			for(int i = 1; i < vPlaces.length; i++)
			{
				vPlaces[i] = vPlaces[i - 1].clone();
				vPlaces[i].x += 117 * driver.autoxs;
			}
			
			float xoff= medals[0][0].width / 2;
			for(int i = 0; i < vLanes.length; i++)
			{
				vLanes[i] = vPlaces[i].clone();
				vLanes[i].x += xoff;
				
				dSlot[i].pos.x = vPlaces[i].x;
				dSlot[i].pos.y = driver.pos.y + 1;
			}
			
			for(int i = 0; i < medals.length; i++)
			{
				for(int j = 0; j < medals[i].length; j++)
				{
					medals[i][j].moveToPlace(j, vPlaces[i]);
				}
			}
			
			fDraw = true;
		}
		
		driver.draw(canvas);
		
		for(int i = 0; i < medals.length; i++)
		{
			if (!inPosition[i])
				dSlot[i].draw(canvas);
			for(int j = 0; j < medals[i].length; j++)
				medals[i][j].draw(canvas);
			if (inPosition[i])
				dSlot[i].draw(canvas);
		}
		
		if (showScanner)
			scanner.draw(canvas);
		for(int i = medals.length - 1; i >= 0; i--)
		{
			if (scanMedal[i] || rLinger[i])
			{
				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
				paint.setStyle(Paint.Style.STROKE);
				paint.setColor(Color.argb(rAlpha[i], rRed[medalId[i]], rGreen[medalId[i]], rBlue[medalId[i]]));
				paint.setStrokeWidth(5);
				Vector p = vLanes[i].clone();
				p = rotate(driver.angle, p, driver.getCenter());
				canvas.drawCircle(p.x,p.y, rRad[i], paint);
				canvas.drawCircle(p.x, p.y, rRad[i] + 7, paint);
				if (scanMedal[i] || !rFull[i])
				{
					rRad[i] += 5;
					if (rRad[i] >= 2.0f * medals[i][medalId[i]].height / 2)
					{
						rRad[i] = (int) (2.0f * medals[i][medalId[i]].height / 2);
						rFull[i] = true;
					}
				}
				if (rLinger[i] && rFull[i])
				{
					rAlpha[i] -= 255 / 60;
					if (rAlpha[i] <= 0)
						rLinger[i] = false;
				}
			}
			else
			{
				rRad[i] = (int) (medals[i][medalId[i]].height / 2);
				rAlpha[i] = 255;
			}
		}
		
		for(int i = 0; i < emblems.length; i++)
		{
			emblems[i].draw(canvas);
		}
		//drawDebug(canvas, false);
	}
	
	@Override
	public void onInitialize() {
		driver = new Driver();
		dSlot = new Driver[3];
		medals = new Medal[3][6];
		emblems = new Emblem[3];
		medalId = new int[3];
		scanner = new OOOScanner();
		
		vPlaces = new Vector[3];
		vLanes = new Vector[3];
		
		driver.init(BitmapFactory.decodeResource(getResources(), R.drawable.ooo_main));
		driver.setXform(this);
		scanner.init(BitmapFactory.decodeResource(getResources(), ResFactory.get("scanner")));
		
		String[] prefixes = {"g", "y", "gr", "r", "b", "p"};
		String[] suffixes = {"t", "m", "b"};
		for(int iSuff = 0; iSuff < suffixes.length; iSuff++)
		{
			dSlot[iSuff] = new Driver();
			dSlot[iSuff].init(BitmapFactory.decodeResource(getResources(), ResFactory.get(suffixes[iSuff] + "slot")));
			
			for(int iPre = 0; iPre < prefixes.length; iPre++)
			{
				Medal m = new Medal();
				Emblem em = new Emblem();
				try
				{
					m.init(BitmapFactory.decodeResource(getResources(), ResFactory.get(prefixes[iPre] + suffixes[iSuff])));
					em.init(BitmapFactory.decodeResource(getResources(), ResFactory.get("c_" + prefixes[iPre] + suffixes[iSuff])));
				}
				catch(Exception e)
				{
					break;
				}
				medals[iSuff][iPre] = m;
			}
		}
	}

	@Override
	public void onUpdate(long gameTime) {
		for(int i = 0; i < medals.length; i++)
		{
			for(int j = 0; j < medals[i].length; j++)
			{
				Medal m = medals[i][j];
				if (m == null)
					continue;
				if (j == medalId[i] && m.pos.y + m.height / 2 < driver.pos.y && setMedal[i] || dragMedal[i])
					inPosition[i] = true;
				m.update();
				
			}
			if (emblems[i] == null) continue;
			Emblem em = emblems[i];
			em.update();
		}
		if (shownEmblems >= 2)
		{
			hideEmblem = true;
		}
		if (hideEmblem)
		{
			ticks += 1;
			if (ticks >= maxTick)
			{
				hideEmblem = false;
				shownEmblems = 0;
				
				for(int i = 0; i < emblems.length; i++)
					emblems[i].fadeOut();
			}
		}
		if (driver != null && driver.angle >= 20)
		{
			canScan = true;
		}
		else
			canScan = false;
	}
	
	public boolean onTouchEvent(MotionEvent event)
	{
		Vector mouse = Vector.create(event.getX(), event.getY());
		int action = event.getAction();
		Vector[] vect;
		Vector begin = Vector.Zero();
		Vector end = Vector.Zero();
		if (canScan)
		{
			vect = Vector.fromArray(new float[] {driver.pos.x, driver.pos.y + driver.height / 2, driver.pos.x + driver.width, driver.pos.y + driver.height / 2});
			begin = rotate(driver.angle, vect[0], driver.getCenter());
			end = rotate(driver.angle, vect[1], driver.getCenter());
		}
		switch(action)
		{
			case MotionEvent.ACTION_DOWN:
			{
				for(int i = 0; i < medals.length; i++)
				{
					for(int j = 0; j < medals[i].length; j++)
					{
						Medal m = medals[i][j];
						if (setMedal[i] && m.place != 0)
							continue;
						if (m.inside(mouse))
						{
							if(setMedal[i])
							{
								dragMedal[i] = true;
								finMedal[i] = false;
							}
							else
								clickMedal[i] = true;
							break;
						}
					}
				}
				if (finMedal[0] && finMedal[1] && finMedal[2])
				{
					if ((medalId[0] != 5 && medalId[1] != 5 && medalId[2] != 5) || 
							(medalId[0] == 5 && medalId[1] == 5 && medalId[2] == 5))
						if (driver.inside(mouse))
						{
							showScanner = false;
							dragDriver = true;
						}
				}
				if (canScan && !dragDriver)
				{
					showScanner = true;
					scanner.setCenter(mouse);
				}
				break;
			}
			case MotionEvent.ACTION_MOVE:
			{
				if (showScanner)
				{
					if (!pScan && !canScanCharge)
					{
						sMgr.playScanner(0);
						pScan = true;
					}
					if (!scanner.scanning)
						scanner.drag(mouse);
					if (begin.varEquals(mouse, 0.2f) || scanner.scanning)
					{
						if (!pScanStart)
						{
							sMgr.playScanner(1, 1);
							pScanStart = true;
						}
						if (canScanCharge)
							isCharge = true;
						scanner.setCenter(begin);
						scanner.scan(mouse, begin, end);
					}
				}
				if (dragDriver && !lckDriver && !scanner.scanning)
				{
					Vector o = driver.getCenter();
					Vector ref;
					double angle;
					boolean onLeft = false;
					if (mouse.x < getWidth() / 2)
					{
						onLeft = true;
						ref = o.clone();
						ref.y = 0;
					}
					else
					{
						ref = o.clone();
						ref.x = getWidth();
					}
					angle = o.Angle(ref, mouse);
					angle = Degrees(angle);
					if (onLeft)
					{
						if (Math.signum(angle) > 0)
							angle = 0;
						else if (angle > -70 && angle < 0)
							angle = -70;
						if (angle < 0)
							angle +=90;
					}
					else
					{
						if (angle < 0)
							angle = 0;
						else if (angle >= 20)
							angle = 20;
					}
					if (Math.floor(angle) != 0)
					{
						driverClosed = false;
					}
					else
					{
						angle = 0;
						driverClosed = true;
						canScanCharge = false;
					}
					//Log.d("Angle", "Angle:" + angle);
					driver.rotate((float) angle, driver.getCenter());
					
					for(int i = 0; i < medals.length; i++)
					{
						Medal m = medals[i][medalId[i]];
						m.rotate((float) angle, driver.getCenter());
						dSlot[i].rotate((float) angle, driver.getCenter());
					}
				}
				for(int i = 0; i < medals.length; i++)
				{
					Medal m = medals[i][medalId[i]];
					if (dragMedal[i] && !dragDriver && driverClosed)
					{
						m.dest = Vector.Zero();
						m.drag(mouse, vLanes[i], false, true);
						if (m.getCenter().varEquals(vLanes[i], 0.2f))
						{
							m.setCenter(vLanes[i]);
						}
					}
					if (showScanner && scanner.scanning)
					{
						if (m.inside(scanner.getCenter()) && !scanMedal[i] && !isCharge)
						{
							if (i != 0)
							{
								if (!scanMedal[i - 1])
									break;
							}
							sMgr.playScanner(i + 2);
							scanMedal[i] = true;
						}
						if (m.inside(scanner.getCenter()) && !chargeMedal[i] && isCharge)
						{
							if (i != 0)
							{
								if (!chargeMedal[i - 1])
									break;
							}
							sMgr.playScanner(i + 2);
							chargeMedal[i] = true;
						}
					}
				}
				break;
			}
			case MotionEvent.ACTION_UP:
			{
				for(int i = 0; i < medals.length; i++)
				{
					if (dragMedal[i])
					{
						Medal m = medals[i][medalId[i]];
						if (m.getCenter().varEquals(vLanes[i], 0.1f))
						{
							finMedal[i] = true;
							sMgr.playSound(0);
						}
						else
						{
							Vector d = Vector.sub(vLanes[i], m.getCenter());
							if (d.length() > driver.height / 2)
							{
								m.animToPlace(0, vPlaces[i]);
								fade(medals[i], true);
								
								setMedal[i] = false;
								inPosition[i] = false;
								
							}
							finMedal[i] = false;
						}
					}
				}
				for(int i = 0; i < medals.length; i++)
				{
					for(int j = 0; j < medals[i].length; j++)
					{
						Medal m = medals[i][j];
						if (m.inside(mouse))
						{
							if (clickMedal[i])
							{
								if (m.place == 0)
								{
									if (!setMedal[i])
									{
										fade(medals[i], false);
										Vector v = vPlaces[i].clone();
										v.x -= m.width / 2;
										v.y = driver.pos.y - m.height * driver.autoys;
										m.dest = v;
										setMedal[i] = true;
									}
									else
									{
										fade(medals[i], true);
										setMedal[i]  = false;
									}
								}
								else
								{
									medalId[i] = scroll(medals[i], m, vPlaces[i].clone(), true);
									break;
								}
							}
						}
						else if(j == medalId[i] && dragMedal[i])
						{
							if (m.getCenter().varEquals(vLanes[i], 0.5f))
							{
								finMedal[i] = true;
								sMgr.playSound(0);
							}
							else
							{
								//sMgr.playExtra(1);
								finMedal[i] = false;
							}
						}
					}
					dragMedal[i] = false;
					clickMedal[i] = false;
				}
				if (showScanner && scanner.scanning)
				{
					if (scanMedal[0] && scanMedal[1] && scanMedal[2])
					{
						sMgr.playScanner(5, 800);
						
						if (!pVO)
						{
							sMgr.playVO(medalId.clone());
							if (medalId[0] == medalId[1] && medalId[0] == medalId[2])
								 sMgr.playCombo(medalId[0]);
							if (medalId[0] == 3 && medalId[1] == 1 && medalId[2] == 0)
								 sMgr.playTaToBa();
							pVO = true;
						}
						sMgr.playSound(2);
						//Unlock the driver
						lckDriver = false;
						//Is Transformed
						isXform = true;
						//Allow scanning charge
						canScanCharge = true;
						
						//The combo vfx is now in lingering state
						rLinger = new boolean[] {true, true, true};
						//OOO rings around the medals are set to false;
						rFull = new boolean[] {false, false, false};
						
						showEmblem();
						Vector p = driver.getCenter();
						p.y = 0;
						//Resets the number of combo segments shown
						shownEmblems = 0;
						//Resets the combo hider
						hideEmblem = false;

						for(int i = 0; i < emblems.length; i++)
						{
							Emblem em = emblems[i];
							em.setListener(this);
							em.show(p.clone());
							p.y += em.height;
							switch(i)
							{
							case 0:
								p.y -= 30 * em.yScale;
								break;
							case 1:
								p.y -= 30 * em.yScale;
								break;
							}
						}
					}
					if (chargeMedal[0] && chargeMedal[1] && chargeMedal[2])
					{
						sMgr.playScanner(5, 800);
						sMgr.playScanner(6, 1);
					}
					for(int b = 0; b < scanMedal.length; b++)
						scanMedal[b] = false;
					for(int b = 0; b < chargeMedal.length; b++)
						chargeMedal[b] = false;
					pScan = false;
					pScanStart = false;
					isCharge = false;
					showScanner = false;
					scanner.stopScan();
				}
				if (reqXform && !pVO)
				 {
					sMgr.playSound(1);
					canScan = true;
					scanner.setCenter(Vector.Zero());
					
					isXform = false;
					lckDriver = false;
					reqXform = false;
				 }
				 if (reqUnxform && !pUnxform)
				 {
					 sMgr.playSound(1);
					 canScan = false;
					 
					 pUnxform = true;
					 lckDriver = false;
					 isXform = false;
					 pVO = false;
					 reqUnxform = false;
				 }
				 dragDriver = false;
				break;
			}
		}
		invalidate();
		return true;
	}

	private double Degrees(double angle) {
		return angle * (180 / Math.PI);
	}
	
	private int scroll(Medal[] list, Medal target, Vector v, boolean reverse)
	{
		int ret = 0;
		int dir = -1;
		if (target.place == 1) dir = 1;
		if (reverse) dir *= -1;
		for(int i = 0; i < list.length; i++)
		{
			Medal m = list[i];
			m.animToPlace(m.place + dir, v);
			if (m.place == 0)
				ret = i;
		}
		return ret;
	}
	
	/**
	 * Fade the list in or out
	 * @param medals2 The list of medals to fade
	 * @param fadeIn Fading in?
	 */
	private void fade(Medal[] medals2, boolean fadeIn)
	{
		for(int i = 0; i < medals2.length; i++)
		{
			Medal m = medals2[i];
			if (m.place != 0)
				if (fadeIn) m.fadeIn();
				else m.fadeOut();
		}
	}
	
	@Override
	public void transform() {
		if (!isXform)
		{
			lckDriver = true;
			reqXform = true;
			pVO = false;
			pScan = false;
		}
	}

	@Override
	public void endTransform() {
		if (isXform)
		{
			lckDriver = true;
			reqUnxform = true;
			pUnxform = false;
			pVO = false;
			pScan = false;
		}
	}
	
	public void drawDebug(Canvas canvas, boolean drawBounds)
	{
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		paint.setStyle(Paint.Style.STROKE);
		
		if (drawBounds)
		{
			for(int i = 0; i < medals.length; i++)
			{
				for(int j = 0; j < medals[i].length; j++)
				{
					Medal m = medals[i][j];
					canvas.drawCircle(m.getCenter().x, m.getCenter().y, m.getRadius(), paint);
				}
			}
		}
		paint.setColor(Color.WHITE);
		for(int i = 0; i < medals.length; i++)
		{
			canvas.drawPoint(vLanes[i].x, vLanes[i].y, paint);
			canvas.drawCircle(vLanes[i].x, vLanes[i].y, 5, paint);
			
			if (emblems[i] != null)
			{
				canvas.drawPoint(emblems[i].pos.x, emblems[i].pos.y, paint);
				canvas.drawCircle(emblems[i].pos.x, emblems[i].pos.y, 5, paint);
			}
		}
		paint.setColor(Color.argb(0xff, 0xff, 0xff, 0xff));
	}
	
	private Vector rotate(float angle, Vector point, Vector o)
	{
		Vector r = new Vector();
		float rad = (float) (angle * (Math.PI / 180));
		r.x = (float) (o.x + Math.cos(rad) * (point.x - o.x) - Math.sin(rad) * (point.y - o.y));
		r.y = (float) (o.y + Math.sin(rad) * (point.x - o.x) + Math.cos(rad) * (point.y - o.y));
		return r;
	}

	@Override
	public void onShowComplete() {
		if (!hideEmblem)
			ticks = 0;
		shownEmblems++;
	}
	
	public void showEmblem()
	{
		String[] prefixes = {"g", "y", "gr", "r", "b", "p"};
		String[] suffixes = {"t", "m", "b"};
		for(int i = 0; i < emblems.length; i++)
		{
			if (emblems[i] == null)
				emblems[i] = new Emblem();
			Emblem em = emblems[i];
			if (i == 0 && medalId[i] == 3 && !isCombo(3))
				em.init(BitmapFactory.decodeResource(getResources(), ResFactory.get("s_taka")));
			else
				em.init(BitmapFactory.decodeResource(getResources(), ResFactory.get("c_" + prefixes[medalId[i]] + suffixes[i])));
			em.setHeight(getHeight(), i);
		}
	}
	
	private boolean isCombo(int id)
	{
		if (medalId[0] != id) return false;
		if (medalId[1] != id) return false;
		if (medalId[2] != id) return false;
		return true;
	}
}
