package com.kamen_rider.ooo_driver;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

public class Emblem
	extends Sprite
{
	private EmblemEvents eListen;
	private boolean isListening = false;
	private int posit = 0;
	
	public void setListener(EmblemEvents l)
	{
		eListen = l;
	}
	
	public void setHeight(float y, int i)
	{
		posit = i;
		float yscale = 1;
		float xscale = 1;
		if (i == 0)
		{
			yscale = (59f / 210f);
			xscale = (137f / 200f);
		}
		if (i == 1)
		{
			yscale = (131f / 210f);
			xscale = (200f / 200f);
		}
		if (i == 2)
		{
			yscale = (76f / 210f);
			xscale = (165f / 200f);
		}
		float nW = y * xscale;
		float nH = y * yscale;
		this.yScale = nH / height;
		height = nH;
		xScale = nW / width;
		width *= xScale;
		pivot = getCenter();
	}
	
	public void init(Bitmap b)
	{
		super.init(b);
		this.alpha = 0;
	}
	public void show(Vector p)
	{
		this.pos = p.clone();
		this.pos.x -= width / 2;
		
		isListening = true;
		fadeIn();
	}
	
	public void draw(Canvas c)
	{
		if (this.alpha >= 0 && image != null)
			super.draw(c);
	}

	public void update()
	{
		super.update();
		if (isListening)
		{
			if (alpha >= 255 && eListen != null)
				eListen.onShowComplete();
		}
	}
	
	public void fadeOut()
	{
		isListening = false;
		super.fadeOut();
	}
	
	public Matrix getMatrix() 
	{
		Matrix m = new Matrix();
		Vector c = getCenter();
		m.setTranslate(pos.x, pos.y);
		m.postScale(xScale, yScale, pos.x, pos.y);
		m.postRotate(angle, pivot.x, pivot.y);
		//TODO: Rotations, scaling
		return m;
	}
}
