package com.kamen_rider.ooo_driver;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

public class OOO_Driver extends Activity
	implements SoundRequester
{
    /** Called when the activity is first created. */
	private henshinSound soundMgr;
	private int vo = 0;
	private int combo = 0;
	private int scan = 0;
	private int tatoba = 0;
	private int extra = 0;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ResFactory.init(this);
        soundMgr = new henshinSound(this);
        initSnd();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(new OOO_Surface(this));
    }
    
    public void initSnd()
    {
    	soundMgr.loadSound(R.raw.ins_medal);
    	soundMgr.loadSound(R.raw.driver);
    	soundMgr.loadSound(R.raw.u_change);
    	extra = soundMgr.loadSound(R.raw.special);
    	soundMgr.loadSound(R.raw.e_accel);
    	soundMgr.loadSound(R.raw.e_trial);
    	vo = soundMgr.loadSound2(R.raw.se_gt);
    	soundMgr.loadSound2(R.raw.se_gm);
    	soundMgr.loadSound2(R.raw.se_gb);
    	soundMgr.loadSound2(R.raw.se_yt);
    	soundMgr.loadSound2(R.raw.se_ym);
    	soundMgr.loadSound2(R.raw.se_yb);
    	soundMgr.loadSound2(R.raw.se_grt);
    	soundMgr.loadSound2(R.raw.se_grm);
    	soundMgr.loadSound2(R.raw.se_grb);
    	soundMgr.loadSound2(R.raw.se_rt);
    	soundMgr.loadSound2(R.raw.se_rm);
    	soundMgr.loadSound2(R.raw.se_rb);
    	soundMgr.loadSound2(R.raw.se_bt);
    	soundMgr.loadSound2(R.raw.se_bm);
    	soundMgr.loadSound2(R.raw.se_bb);
    	soundMgr.loadSound2(R.raw.se_pt);
    	soundMgr.loadSound2(R.raw.se_pm);
    	soundMgr.loadSound2(R.raw.se_pb);
    	tatoba = soundMgr.loadSound(R.raw.c_ryg);
    	combo = soundMgr.loadSound(R.raw.c_ggg);
    	soundMgr.loadSound(R.raw.c_yyy);
    	soundMgr.loadSound(R.raw.c_grgrgr);
    	soundMgr.loadSound(R.raw.c_rrr);
    	soundMgr.loadSound(R.raw.c_bbb);
    	soundMgr.loadSound(R.raw.c_ppp);
    	scan = soundMgr.loadSound2(R.raw.scanner);
    	soundMgr.loadSound2(R.raw.scan_start);
    	soundMgr.loadSound2(R.raw.scan_00);
    	soundMgr.loadSound2(R.raw.scan_01);
    	soundMgr.loadSound2(R.raw.scan_02);
    	soundMgr.loadSound2(R.raw.scan_end);
    	soundMgr.loadSound2(R.raw.scanning_charge);
    }

	@Override
	public void playSound(int id) {
		soundMgr.playSound(id);
	}
	
	public void playSound(int id, int d) {
		soundMgr.qSound(id, d);
	}

	@Override
	public void playVO(int[] medals) {
		//int[] lim = {3, 2, 2};
		for(int i = 0; i < medals.length; i++)
		{
			int id = vo + i + medals[i] * 3;
			soundMgr.qSound(id, 800);
		}
	}

	@Override
	public void playScanner(int scanner) {
		soundMgr.playSound(scanner + scan);
	}
	
	public void playScanner(int scanner, int d) {
		soundMgr.qSound(scanner + scan, d);
	}

	@Override
	public void playCombo(int color) {
		soundMgr.qSound(combo + color);
	}

	@Override
	public void playTaToBa() {
		soundMgr.qSound(tatoba);
	}

	@Override
	public void playExtra(int i) {
		soundMgr.playSound(i + extra);
	}
}