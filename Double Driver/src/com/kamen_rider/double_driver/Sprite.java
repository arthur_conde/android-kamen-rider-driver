package com.kamen_rider.double_driver;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class Sprite
{
	private Bitmap image;
	private float xPos;
	private float yPos;
	private Rect sRect;
	private Vertex pos;
	
	public int height;
	public int width;
	public Vertex origin;
	public boolean bSet;
	
	private HenshinEvent henshin;
	
	public Sprite(){
		sRect = new Rect(0,0,0,0);
		xPos = 0;
		yPos = 0;
	}
	
	public void Initialize(Bitmap bitmap, int height, int width) {
		this.image = bitmap;
		this.height = this.image.getHeight();
		this.width = this.image.getWidth();
		this.sRect.top = 0;
		this.sRect.bottom = this.height;
		this.sRect.left = 0;
		this.sRect.right = this.width;
		
		this.bSet = true;
	}
	
	public void setListener(HenshinEvent h)
	{
		henshin = h;
	}

	public float getX() {
		return xPos;
	}
	
	public float getY() {
		return yPos;
	}
	
	public void setXPos(float value) {
		xPos = value;
	}
	
	public void setYPos(float value) {
		yPos = value;
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getWidth(){
		return width;
	}
	public void draw(Canvas canvas) {
		if (this.image == null)
			return;
		Paint m_paint = new Paint();
		m_paint.setAntiAlias(true);
		m_paint.setFilterBitmap(true);
		//if (getMatrix() != null)
			canvas.drawBitmap(this.image, getMatrix(), m_paint);
		/*else
			canvas.drawBitmap(this.anim, getX(), getY(), m_paint);*/
	}
	
	public void rotate(float angle)
	{
		if (Math.floor(angle) == 0)
		{
			henshin.unHenshin();
			angle = 0;
		}
		if (angle >= 45)
		{
			henshin.henshin();
			angle = 45;
		}
		if (angle <= -45)
		{
			henshin.henshin();
			angle = -45;
		}
		
		this.angle = angle;
		//this.anim = Bitmap.createBitmap(this.orig, 0, 0, w, h, m, true);
	}
	
	public Vertex getCenter()
	{
		return Vertex.create(pos.x + width / 2, pos.y + height / 2);
	}
	
	private float angle = 0.0f;
	
	private Matrix getMatrix()
	{
		Matrix m = new Matrix();
		pos = getPos();
		Vertex cen = getCenter();
		m.setTranslate(pos.x, pos.y);
		m.postScale(1.5f, 1.5f, cen.x, cen.y);
		width = (int) (1.5f * this.image.getWidth());
		height = (int) (1.5f * this.image.getHeight());
		if (origin != null)
			m.postRotate(angle, pos.x + origin.x, pos.y + origin.y);
		return m;
	}
	
	public Vertex getPivot()
	{
		return Vertex.create(pos.x + origin.x, pos.y + origin.y);
	}
	
	public void setPos(Vertex v)
	{
			this.xPos = v.x;
			this.yPos = v.y;
	}
	
	public void setCentered(Vertex v)
	{
		this.xPos = v.x - width;
		this.yPos = v.y - height;
	}
	
	public RectF getRect()
	{
		return new RectF
			(
					this.xPos,
					this.yPos,
					this.xPos + width,
					this.yPos + height
			);
	}
	
	public RectF getSimpleRect()
	{
		return new RectF
			(
					0,
					0,
					width,
					height
			);
	}
	
	public Vertex getPos()
	{
		return Vertex.create(this.xPos, this.yPos);
	}

	public void setPos(int i, int j) {
		setPos(Vertex.create(i,j));
	}

	public void setPos(float f, float g) {
		setPos(Vertex.create(f, g));
		
	}
	
	public RectF getTrueBounds()
	{
		RectF bounds = this.getSimpleRect();
		getMatrix().mapRect(bounds);
		return bounds;
	}
	
	public boolean inside(Vertex v)
	{
		RectF bounds = this.getTrueBounds();
		if (v.x < bounds.left || v.x > bounds.right)
		{
			return false;
		}
		if (v.y < bounds.top || v.y > bounds.bottom)
		{
			return false;
		}
		return true;
	}
}
