package com.kamen_rider.double_driver;


public class Vertex {
	public float x;
	public float y;
	public Vertex()
	{
		
	}
	
	public boolean Zero(){
		if (x == 0 && y == 0)
			return true;
		return false;
	}
	
	public Vertex sub(Vertex a)
	{
		return Vertex.create(x - a.x, y - a.y);
	}
	
	public void abs()
	{
		x = Math.abs(x);
		y = Math.abs(y);
	}
	
	public boolean equals(Vertex a)
	{
		if (a.x == x && a.y == y)
			return true;
		return false;
	}
	
	public boolean varEquals(Vertex a, float error)
	{
		if (Math.abs((this.x - a.x) / a.x) > error)
			return false;
		if (Math.abs((this.y - a.y) / a.y) > error)
			return false;
		return true;
	}
	
	/**
	 * Checks if a is within error to this
	 * @param a The vertex to compare
	 * @param error The minimum error
	 * @return True if a is too far away from this, false otherwise
	 */
	public boolean revError(Vertex a, float error)
	{
		if (Math.abs((this.x - a.x) / a.x) < error)
			return false;
		if (Math.abs((this.y - a.y) / a.y) < error)
			return false;
		return true;
	}
	
	public static boolean equals(Vertex a, Vertex b)
	{
		if (a.x == b.x && a.y == b.y)
			return true;
		return false;
	}
	
	/**
	 * Returns the direction in terms of left or right
	 * @param A The vertex of the line
	 * @return True if facing right, false if facing left
	 */
	public boolean dir(Vertex A)
	{
		if (A.x > this.x)
			return true;
		else if (A.x < this.x)
			return false;
		return false;
	}
	
	public double Angle(Vertex A, Vertex B)
	{
		Vertex L1 = Vertex.sub(this, A);
		Vertex L2 = Vertex.sub(this, B);
		double atan2 = Math.atan2(L2.y, L2.x) - Math.atan2(L1.y, L1.x);
		@SuppressWarnings("unused")
		double angle = Math.acos(Vertex.Dot(L1, L2) / (L1.length() * L2.length()));
		return atan2;
	}
	
	public double length()
	{
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	 @Override
	 public String toString() {
	     return "[" + x + ", " + y + "]";
	   }
	
	public Vertex clone()
	{
		return Vertex.create(x,y);
	}
	
	public static Vertex create(float x, float y)
	{
		Vertex v = new Vertex();
		v.x = x;
		v.y = y;
		return v;
	}
	
	public static Vertex createRotate(float degrees, Vertex point, Vertex origin)
	{
		Vertex v = new Vertex();
		double d = degrees * (Math.PI / 180);
		v.x = (float)(origin.x + ((point.x - origin.x) * Math.cos(d) - (point.y - origin.y) * Math.sin(d)));
		v.y = (float)(origin.y + ((point.x - origin.x) * Math.sin(d) + (point.y - origin.y) * Math.cos(d)));
		return v;
	}
	
	/**
	 * Subtracts b from a
	 * @param a The vertex to subtract
	 * @param b The vertex to subtract from
	 * @return The result of the subtraction
	 */
	public static Vertex sub(Vertex a, Vertex b)
	{
		return Vertex.create(b.x - a.x, b.y - a.y);
	}
	
	public static float Dot(Vertex a, Vertex b)
	{
		return (a.x * b.x) + (a.y * b.y);
	}
	
	public static double length(Vertex a, Vertex b)
	{
		//return Math.sqrt(Math.pow(a.x, 2) + Math.pow(a.y, 2)) * Math.sqrt(Math.pow(b.x, 2) + Math.pow(b.y, 2));
		return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
	}
	
	public Vertex Rotate(Vertex origin, float degrees)
	{
		Vertex v = new Vertex();
		double rad = degrees * (Math.PI / 180);
		v.x = (float) (origin.x + ((x - origin.x) * Math.cos(rad) - (y - origin.y) * Math.sin(rad)));
		v.y = (float) (origin.y + ((x - origin.x) * Math.sin(rad) + (y - origin.y) * Math.cos(rad)));
		return v;
	}
	
	public static Vertex add(Vertex A, Vertex B)
	{
		return Vertex.create(A.x + B.x, A.y+ B.y);
	}
}
