package com.kamen_rider.double_driver;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

public class GaiaMemory {
	private Bitmap orig;
	private Bitmap anim;
	private Vertex pos;
	/**
	 * Pivot point for angle
	 */
	private Vertex pivot;
	private float angle;
	/**
	 * Do not use to write to position vector
	 */
	private RectF bounds;
	private float xs;
	private float ys;
	private Vertex dest;
	
	public int height;
	public int width;
	public int soundId;
	public int place;
	
	static final float xscale = 1.2f;
	static final float yscale = 1.2f;
	
	public GaiaMemory()
	{
		this.pos = Vertex.create(0, 0);
		
	}

	public void setPos(Vertex pos) {
		this.pos = pos;
		this.dest = this.pos;
		updateBounds();
	}

	public Vertex getPos() {
		return pos;
	}
	
	public void setCenter(Vertex pos)
	{
		this.pos = Vertex.create(pos.x - width / 2, pos.y - height / 2);
		this.dest = this.pos;
		updateBounds();
	}
	
	private Matrix getMatrix()
	{
		Vertex scale_center = this.getCenter();
		Matrix m = new Matrix();
		
		m.setTranslate(this.pos.x, this.pos.y);
		m.postScale(xs, ys, scale_center.x, scale_center.y);
		/*
		if (place == 0)
			m.postScale(xscale, yscale, scale_center.x, scale_center.y);
		else
			m.postScale(0.8f, 0.8f, scale_center.x, scale_center.y);
		*/
		if (pivot != null)
			m.postRotate(angle, pivot.x, pivot.y);
		return m;
	}
	
	private boolean animate = false;
	private int fadeMode = 0;
	private int alpha = 255;
	
	public void fadeIn()
	{
		fadeMode = 1;
	}
	
	public void fadeOut()
	{
		fadeMode = 2;
	}
	
	private void updateBounds()
	{
		this.bounds = new RectF(pos.x, pos.y, pos.x + width, pos.y + height);
	}
	
	public Vertex getCenter()
	{
		return Vertex.create(pos.x + width / 2, pos.y + height / 2);
	}
	
	public void init(Bitmap bitmap, int sId)
	{
		this.soundId = sId;
		
		this.orig = bitmap;
		this.anim = bitmap;
		
		this.width = this.anim.getWidth();
		this.height = this.anim.getHeight();
		
		this.pos.x -= width / 2;
		this.pos.y -= height / 2;
		
		this.xs = 1.0f;
		this.ys = 1.0f;
	}
	
	public void draw(Canvas canvas)
	{
		if (this.anim == null)
			return;
		Paint m_paint = new Paint();
		m_paint.setAntiAlias(true);
		m_paint.setFilterBitmap(true);
		m_paint.setAlpha(alpha);
		canvas.drawBitmap(this.anim, getMatrix(), m_paint);
		getMatrix().mapRect(bounds);
	}
	
	static final int frames = 30;
	public void update()
	{
		if (dest == null)
			dest = getPos();
		
		if (fadeMode == 1)
		{
			alpha += 255 / frames;
			if (alpha >= 255)
			{
				alpha = 255;
				fadeMode = 0;
			}
		}
		if (fadeMode == 2)
		{
			alpha -= 255 / frames;
			if (alpha <= 0)
			{
				alpha = 0;
				fadeMode = 0;
			}
		}
		
		if (!pos.equals(dest))
		{
			float dX = (this.dest.x - this.pos.x) / frames;
			float dY = (this.dest.y - this.pos.y) / frames;
			this.pos.x += dX;
			this.pos.y += dY;
		}
		if (this.xs > 0.8f && this.place != 0 || this.xs < xscale && this.place == 0)
		{
			float xts = this.place == 0 ? xscale : 0.8f;
			float dxs = (xts - 1.0f) / frames;
			this.xs += dxs;
		}
		if (this.ys > 0.8f && this.place != 0 || this.ys < yscale && this.place == 0)
		{
			float yts = this.place == 0 ? yscale : 0.8f;
			float dys = (yts - 1.0f) / frames;
			this.ys += dys;
		}
	}
	
	public void setMemory(Vertex pos)
	{
		updateBounds();
		
		this.pos = pos.clone();
		this.pos.y = 0 - height;
		
		this.dest = pos;
	}
	
	public void remMemory(Vertex pos)
	{
		pos.x -= width / 2;
		pos.y -= height / 2;
		updateBounds();
		this.pos = pos.clone();
		this.pos.y = 0 - height;
		
		this.dest = pos;
	}
	
	/**
	 * Move this object to the specified place
	 * @param place Center = 0, Left = 1, Right = 2
	 */
	public void setPlace(int place, Vertex pos)
	{
		place %= 3;
		if (place < 0)
			place += 3;
		if (place != 0)
		{
			//this.anim = Bitmap.createScaledBitmap(this.orig, dstWidth, dstHeight, true);
			
			float xOffset = this.orig.getWidth() * xscale;
			float yOffset = this.orig.getHeight() * yscale / -2;
			if (place == 1)
			{
				//To the left
				xOffset /= -1;
			}
			else
			{
				//To the right
				xOffset /= 1;
			}
			pos.x += xOffset;
			pos.y += yOffset;
		}
		else
		{
			this.anim = this.orig;
		}
		pos.x -= width / 2;
		pos.y -= height / 2;
		
		this.place = place;
		
		if (!animate)
		{
			this.pos = pos;
			xs = this.place == 0 ? xscale : 0.8f;
			ys = this.place == 0 ? yscale : 0.8f;
		}
		this.dest = pos;
		
		updateBounds();
	}
	
	public void setPlace(int place, Vertex pos, boolean animate)
	{
		this.animate = animate;
		setPlace(place, pos);
	}
	
	private boolean isTrimmed = false;
	public void toggleTrim()
	{
		if (!isTrimmed)
		{
			this.anim = Bitmap.createBitmap(orig, 0, 0, orig.getWidth(), (int) (orig.getHeight() - (40 * ys)));
			isTrimmed = true;
		}
		else
		{
			this.anim = Bitmap.createBitmap(orig, 0, 0, orig.getWidth(), orig.getHeight());
			isTrimmed = false;
		}
	}
	
	/**
	 * Allows dragging of GaiaMemory
	 * @param mouse The position of the mouse as a vertex
	 * @param center The aligned position
	 * @param useX Allow changes in X position
	 * @param useY Allow changes in Y position
	 */
	public void drag(Vertex mouse, Vertex center, boolean useX, boolean useY)
	{
		Vertex nPos = center.clone();
		if (useX)
			nPos.x = Math.min(center.x, mouse.x - (width / 2));
		if  (useY)
			nPos.y = Math.min(center.y, mouse.y - (height / 5));
		this.pos = nPos;
		Log.d("GaiaMemory::drag", "[" + nPos.x + ", " + nPos.y + "]");
		updateBounds();
	}
	
	/**
	 * Gets bounds at place 0
	 * @param pos
	 * @return
	 */
	public RectF getBounds(Vertex pos)
	{
		return new RectF(pos.x, pos.y, pos.x + width, pos.y + height);
	}
	
	public RectF getTrueBounds()
	{
		RectF rReturn = new RectF(0,0,width, height);
		getMatrix().mapRect(rReturn);
		return rReturn;
	}
	
	public RectF getBound()
	{
		return bounds;
	}
	
	/**
	 * Checks if the point v is inside the box
	 * @param v Vertex containing coords
	 * @return
	 */
	public boolean inside(Vertex v)
	{
		RectF bounds = new RectF(0,0,width, height);
		getMatrix().mapRect(bounds);
		if (v.x < bounds.left || v.x > bounds.right)
		{
			return false;
		}
		if (v.y < bounds.top || v.y > bounds.bottom)
		{
			return false;
		}
		return true;
	}
	
	public void rotate(float angle, Vertex origin)
	{
		if (Math.floor(angle) == 0)
		{
			angle = 0;
		}
		if (angle >= 45)
		{
			angle = 45;
		}
		if (angle <= -45)
		{
			angle = -45;
		}
		this.angle = angle;
		this.pivot = origin.clone();
	}
}
