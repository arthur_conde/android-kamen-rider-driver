package com.kamen_rider.double_driver;

import android.graphics.Rect;
import android.graphics.RectF;

public class Box {
	public Vertex tl;
	public Vertex tr;
	public Vertex bl;
	public Vertex br;
	public Box(){
		tl = new Vertex();
		tr = new Vertex();
		bl = new Vertex();
		br = new Vertex();
	}
	
	public RectF getBounds()
	{
		return new RectF(
				Math.min(Math.min(bl.x, br.x), Math.min(tl.x, tr.x)),
				Math.min(Math.min(bl.y, br.y), Math.min(tl.y, tr.y)),
				Math.max(Math.max(bl.x, br.x), Math.max(tl.x, tr.x)),
				Math.max(Math.max(bl.y, br.y), Math.max(tl.y, tr.y))
				);
	}
	
	public static Box create(Vertex tl,Vertex tr,Vertex bl,Vertex br)
	{
		Box b = new Box();
		b.tl = tl;
		b.tr = tr;
		b.bl = bl;
		b.br = br;
		return b;
	}
	
	public static Box create(Rect r)
	{
		Box b = new Box();
		b.tl = Vertex.create((float)r.top, (float)r.left);
		b.tr = Vertex.create((float)r.top, (float)r.right);
		b.bl = Vertex.create((float)r.bottom, (float)r.left);
		b.br = Vertex.create((float)r.bottom, (float)r.right);
		return b;
	}

	public Box create(RectF rect) {
		Box b = new Box();
		b.tl = Vertex.create(rect.top, rect.left);
		b.tr = Vertex.create(rect.top, rect.right);
		b.bl = Vertex.create(rect.bottom, rect.left);
		b.br = Vertex.create(rect.bottom, rect.right);
		return b;
	}
}
