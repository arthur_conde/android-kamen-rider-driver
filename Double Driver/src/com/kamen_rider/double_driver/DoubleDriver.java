package com.kamen_rider.double_driver;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class DoubleDriver extends DrawablePanel
	implements HenshinEvent
{
	Context m_ctx;
	private Sprite Plate;
	private Sprite Ele;
	private Sprite Style;
	
	private GaiaMemory cyclone;
	private GaiaMemory heat;
	private GaiaMemory luna;
	private GaiaMemory joker;
	private GaiaMemory metal;
	private GaiaMemory trigger;
	
	private GaiaIcon giElement;
	private GaiaIcon giStyle;
	
	private playHenshin soundMgr;
	/**
	 * Has the Driver change sfx been played
	 */
	private boolean hasPlayed = false;
	/**
	 * Have we received a request to change
	 */
	private boolean hasChanged = false;
	
	private ArrayList<GaiaMemory> lElement;
	private ArrayList<GaiaMemory> lStyle;
	
	public DoubleDriver(Context ctx, playHenshin mgr){
		super(ctx);
		soundMgr = mgr;
		m_ctx = ctx;
		
		LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
		this.setLayoutParams(lp);
	}
	
	private boolean inFlux = false;
	public void henshin()
	{
		if (!hasChanged)
		{
			soundMgr.playSound(0);
			hasChanged = true;
			inFlux = true;
		}
	}
	
	private boolean remChange = false;
	private boolean remPlayed = false;
	
	public void unHenshin()
	{
		if (hasChanged)
		{
			remChange = true;
			inFlux = true;
		}
	}
	
	private void drawMemories(Canvas canvas)
	{
		GaiaMemory[] memories = new GaiaMemory[6];
		for(int iE = 0; iE < lElement.size(); iE++)
		{
			GaiaMemory gm = lElement.get(iE);
			memories[gm.place] = gm;
		}
		for(int iS = 0; iS < lStyle.size(); iS++)
		{
			GaiaMemory gm = lStyle.get(iS);
			memories[gm.place + 3] = gm;
		}
		for(int i = memories.length - 1; i >= 0 ; i--)
		{
			int j = i;
			if (i == 1 && revElement)
				j = 2;
			if (i == 2 && revElement)
				j = 1;
			if (i == 4 && revStyle)
				j = 5;
			if (i == 5 && revStyle)
				j = 4;
			memories[j].draw(canvas);
		}
	}
	
	private void moveMe(int x, int y)
	{
		Vertex center = Vertex.create(x - (Plate.width / 2), y - (Plate.height / 2));
		Vertex left = Vertex.create(x - (Style.width / 2), y - (Style.height / 2));
		Vertex right = Vertex.create(x - (Ele.width / 2), y - (Ele.height / 2));
		
		int ox = 98;
		int oy = 95;
		
		left.x = left.x + ox;
		left.y = left.y - oy;
		right.x = right.x - ox;
		right.y = right.y - oy;
		
		Plate.setPos(center);
		Style.setPos(left);
		Ele.setPos(right);
		Log.d("moveMe","X: " + x + " Y: " + y);
	}
	
	private boolean firstDraw = true;
	@Override
	public void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		canvas.drawColor(Color.BLACK);
		if (firstDraw)
		{
			moveMe(getWidth() / 2, getHeight() / 5 * 4);
			Vertex left =  Vertex.create(getWidth() / 8, getHeight() / 3 * 2);
			Vertex right =  Vertex.create(getWidth() / 8 * 7, getHeight() / 3 * 2);
			
			cyclone.setPlace(0,  left.clone());
			heat.setPlace(1,  left.clone());
			luna.setPlace(2,  left.clone());
			joker.setPlace(0, right.clone());
			metal.setPlace(1, right.clone());
			trigger.setPlace(2, right.clone());
			
			firstDraw = false;
		}
		
		/*
		cyclone.draw(canvas);
		heat.draw(canvas);
		luna.draw(canvas);
		joker.draw(canvas);
		metal.draw(canvas);
		trigger.draw(canvas);
		*/
		
		drawMemories(canvas);
		
		Ele.draw(canvas);
		Style.draw(canvas);
		
		Plate.draw(canvas);
		
		try
		{
			giElement.draw(canvas);
			giStyle.draw(canvas);
			
			///*
			Paint myPaint = new Paint();
			myPaint.setColor(Color.YELLOW);
			//canvas.drawPoint(v.x, v.y, myPaint);
			//*/
		}
		catch (Exception e)
		{}
	}
	
	@Override
	public void onInitialize() {
		Plate = new Sprite();
		Ele = new Sprite();
		Style = new Sprite();
		
		joker = new GaiaMemory();
		metal = new GaiaMemory();
		trigger = new GaiaMemory();
		cyclone = new GaiaMemory();
		heat = new GaiaMemory();
		luna = new GaiaMemory();
		
		giElement = new GaiaIcon();
		giStyle = new GaiaIcon();
		
		lElement = new ArrayList<GaiaMemory>();
		lStyle = new ArrayList<GaiaMemory>();
		
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inScaled = false;
		
		Ele.Initialize(BitmapFactory.decodeResource(getResources(), R.drawable.left, opt), 145, 158);
		Style.Initialize(BitmapFactory.decodeResource(getResources(), R.drawable.right, opt), 144, 153);
		Plate.Initialize(BitmapFactory.decodeResource(getResources(), R.drawable.plate, opt), 106, 138);
		
		cyclone.init(BitmapFactory.decodeResource(getResources(), R.drawable.gm_cyclone), 3);
		heat.init(BitmapFactory.decodeResource(getResources(), R.drawable.gm_heat), 4);
		luna.init(BitmapFactory.decodeResource(getResources(), R.drawable.gm_luna), 5);
		joker.init(BitmapFactory.decodeResource(getResources(), R.drawable.gm_joker), 6);
		metal.init(BitmapFactory.decodeResource(getResources(), R.drawable.gm_metal), 7);
		trigger.init(BitmapFactory.decodeResource(getResources(), R.drawable.gm_trigger), 8);
		
		lElement.add(cyclone);
		lElement.add(heat);
		lElement.add(luna);
		lStyle.add(joker);
		lStyle.add(metal);
		lStyle.add(trigger);
		
		Ele.setListener(this);
		Style.setListener(this);
		
		Style.origin = Vertex.create(Style.width * 0.16f, Style.height * .93f);
		Ele.origin = Vertex.create(Ele.width * 0.61f, Ele.height * .93f);
		
		Log.d("Bitmap", "Ele.height: " + Ele.getHeight() + " Ele.width:" +  Ele.getWidth());
	}

	@Override
	public void onUpdate(long gameTime) 
	{
		try
		{
			giElement.update();
			giStyle.update();
			
			for(int iE = 0; iE < lElement.size(); iE++)
			{
				GaiaMemory gm = lElement.get(iE);
				gm.update();
			}
			for(int iS = 0; iS < lStyle.size(); iS++)
			{
				GaiaMemory gm = lStyle.get(iS);
				gm.update();
			}
		}
		catch(Exception e)
		{
			
		}
	}
	
	/**
	 * Enables opening and closing the Driver
	 */
	private boolean usingDriver = false;
	private boolean usingElement = false;
	private boolean usingStyle = false;
	/**
	 * Are we setting an Element
	 */
	private boolean elementToFront = false;
	/**
	 * Are we setting a Style
	 */
	private boolean styleToFront = false;
	
	/**
	 * Has an Element been moved to the Driver
	 */
	private boolean elementInDriver = false;
	/**
	 * Has a Style been moved to the Driver
	 */
	private boolean styleInDriver = false;
	/**
	 * Can we drag the Element?
	 */
	private boolean elementSetInDriver = false;
	/**
	 * Can we drag the Style
	 */
	private boolean styleSetInDriver = false;
	/**
	 * Finalize Element. The element should be in position.
	 */
	private boolean elementSet = false;
	/**
	 * Finalize Style
	 */
	private boolean styleSet = false;
	
	private boolean lockMemory = false;
	/**
	 * Lock memories in place. Use while not henshin
	 */
	private boolean lockMemories = false;
	
	private int iElement = 0;
	private int iStyle = 0;
	private Vertex firstClick;
	public boolean onTouchEvent(MotionEvent event){
		int eID = event.getAction();
		int x = (int)event.getX();
		@SuppressWarnings("unused")
		int y = (int)event.getY();
		Vertex click = Vertex.create(event.getX(),event.getY());
		
		Vertex left =  Vertex.create(getWidth() / 8, getHeight() / 3 * 2);
		Vertex right =  Vertex.create(getWidth() / 8 * 7, getHeight() / 3 * 2);
		
		Vertex v_left = Vertex.create(Ele.getX() + 40, Ele.getY() - Ele.height / 3);
		Vertex v_right = Vertex.create(Style.getX() - 17, Style.getY() - Style.height / 3);
		
		int mem_oy = 30;
		Vertex t_left = Vertex.create(v_left.x, v_left.y + mem_oy);
		Vertex t_right = Vertex.create(v_right.x, v_right.y + mem_oy - 1);
		
		switch(eID)
		{
			case MotionEvent.ACTION_DOWN:
			{
				firstClick = click;
				//TODO: ACTION_DOWN Top
				for(int iE = 0; iE < lElement.size(); iE++)
				{
					if (lockMemory || lockMemories)
						continue;
					GaiaMemory gm = lElement.get(iE);
					if (gm.inside(click))
					{
						//If we have moved a memory to the driver, enable dragging and prevent opening/closing the driver
						if (elementInDriver)
						{
							elementSetInDriver = true;
							elementSet = false;
						}
						//If the memory in front is this memory, 
						else if(iElement == iE)
							usingElement = true;
						else if(iElement != iE && !hasChanged && !elementInDriver)
							elementToFront = true;
					}
				}
				for(int iS = 0; iS < lStyle.size(); iS++)
				{
					if (lockMemory || lockMemories)
						continue;
					GaiaMemory gm = lStyle.get(iS);
					if (gm.inside(click))
					{
						if (styleInDriver)
						{
							styleSetInDriver = true;
							styleSet = false;
						}
						else if(iStyle == iS)
							usingStyle = true;
						else if(iStyle != iS && !hasChanged && !styleInDriver)
							styleToFront = true;
					}
				}
				//TODO: usingDriver
				if (Ele.inside(click) == true || Style.inside(click) == true)
				{
					if (elementSet && styleSet)
					{
						usingDriver = true;
					}
				}
				break;
			}
			case MotionEvent.ACTION_MOVE:
			{
				GaiaMemory gm_s = lStyle.get(iStyle);
				GaiaMemory gm_e = lElement.get(iElement);
				if (!firstClick.equals(click))
				{
					Vertex line = Vertex.sub(firstClick, click);
					double d = Math.abs(line.x) - Math.abs(line.y);
					if (d > 0)
					{
						if (elementSet && styleSet)
							usingDriver = true;
					}
				}
				if (elementSetInDriver && !usingDriver && !hasChanged)
				{
					gm_e.drag(click, t_left, false, true);
					if (gm_e.getPos().varEquals(t_left, 0.30f))
					{
						gm_e.setPos(t_left.clone());
					}
				}
				if (styleSetInDriver && !usingDriver && !hasChanged)
				{
					gm_s.drag(click, t_right, false, true);
					if (gm_s.getPos().varEquals(t_right, 0.30f))
					{
						gm_s.setPos(t_right.clone());
					}
				}
				if (usingDriver && !inFlux)
				{
					lockMemories = true;
					
					Vertex o;
					Vertex ref;
					double angle;
					if (x < getWidth() / 2)
					{
						o = Vertex.add(Ele.origin, Ele.getPos());
						ref = o.clone();
						ref.y = 0;
					}
					else
					{
						o = Vertex.add(Style.origin, Style.getPos());
						ref = o.clone();
						ref.y = 0;
					}
					angle = o.Angle(ref, click);
					angle = Degrees(angle);
					if (x < getWidth() / 2)
					{
						if (angle > 0)
							angle = 0;
						else
							angle *= -1;
					}
					else
					{
						if (angle < 0)
							angle = 0;
					}
					Ele.rotate((float) -angle);
					Style.rotate((float) angle);
					if (elementSet)
						gm_e.rotate((float) -angle, Vertex.add(Ele.origin, Ele.getPos()));
					if (styleSet)
						gm_s.rotate((float) angle, Vertex.add(Style.origin, Style.getPos()));
				
					Log.d("onTouchEvent", "Angle: " + angle);
				}
				break;
			}
			case MotionEvent.ACTION_UP:
			{
				if (elementSetInDriver)
				{
					GaiaMemory gm = lElement.get(iElement);
					if (gm.getPos().varEquals(t_left, 0.15f))
					{
						soundMgr.playSound(1);
						elementSet = true;
					}
					else if(!gm.getPos().revError(t_left, 0.30f))
					{
						if (!hasChanged && !usingDriver)
						{
							gm.remMemory(left.clone());
							fadeMemory(lElement, false);
							gm.toggleTrim();
							elementInDriver = false;
							elementSetInDriver = false;
							elementSet = false;
						}
					}
					else
						elementSet = false;
				}
				if (styleSetInDriver)
				{
					GaiaMemory gm = lStyle.get(iStyle);
					if (gm.getPos().varEquals(t_right, 0.15f))
					{
						soundMgr.playSound(1);
						styleSet = true;
					}
					else if(!gm.getPos().revError(t_right, 0.30f))
					{
						if (!hasChanged && !usingDriver)
						{
							gm.remMemory(right.clone());
							fadeMemory(lStyle, false);
							gm.toggleTrim();
							styleInDriver = false;
							styleSetInDriver = false;
							styleSet = false;
						}
					}
					else
						styleSet = false;
				}
				for(int iE = 0; iE < lElement.size(); iE++)
				{
					GaiaMemory gm = lElement.get(iE);
					if (gm.inside(click))
					{
						if (usingElement)
						{
							if (iElement == iE)
							{
								if (!hasChanged)
								{
									gm.setMemory(v_left.clone());
									fadeMemory(lElement, true);
									gm.toggleTrim();
									elementInDriver = true;
									soundMgr.playSound(gm.soundId);
									break;
								}
							}
							else if (!elementSetInDriver && !elementToFront)
							{	
								rotateElements(gm, left.clone());
								iElement = iE;
								break;
							}
						}
						if (elementToFront)
						{
							if (elementInDriver)
								continue;
							rotateElements(gm, left.clone());
							soundMgr.playSound(gm.soundId);
							iElement = iE;
							break;
						}
					}
				}
				for(int iS = 0; iS < lStyle.size(); iS++)
				{
					GaiaMemory gm = lStyle.get(iS);
					if (gm.inside(click))
					{
						if (usingStyle)
						{
							if (iStyle == iS)
							{
								if (!hasChanged)
								{
									gm.setMemory(v_right.clone());
									fadeMemory(lStyle, true);
									gm.toggleTrim();
									styleInDriver = true;
									soundMgr.playSound(gm.soundId);
									break;
								}
							}
							else if (!styleSetInDriver && !styleToFront)
							{
								rotateStyles(gm, right.clone());
								iStyle = iS;
								break;
							}
						}
						if (styleToFront)
						{
							if (styleInDriver)
								continue;
							rotateStyles(gm, right.clone());
							soundMgr.playSound(gm.soundId);
							iStyle = iS;
							break;
						}
					}
				}
				
				usingDriver = false;
				usingStyle = false;
				usingElement = false;
				styleSetInDriver = false;
				elementSetInDriver = false;
				
				if (hasChanged && !hasPlayed)
				{
					showIcon(iElement, iStyle);
					soundMgr.playSound(iElement, iStyle);
					hasPlayed = true;
					
					remPlayed = false;
					remChange = false;
					
					inFlux = false;
					lockMemory = true;
					lockMemories = false;
				}
				else if (remChange && !remPlayed){
					soundMgr.playSound(2);
					remPlayed = true;
					
					hasChanged = false;
					hasPlayed = false;
					
					inFlux = false;
					lockMemory = false;
					lockMemories = false;
				}
				break;
			}
		}
		invalidate();
		return true;
	}
	
	private boolean revElement = false;
	private boolean revStyle = false;
	private void rotateElements(GaiaMemory gm, Vertex pos)
	{
		if (gm.place == 1)
		{
			revElement = true;
			//If we are on the left
			for(Iterator<GaiaMemory> ele = lElement.iterator(); ele.hasNext();)
			{
				GaiaMemory gm_ele = ele.next();
				gm_ele.setPlace(gm_ele.place - 1, pos.clone(), true);
			}
		}
		else
		{
			revElement = false;
			//On the right
			for(Iterator<GaiaMemory> ele = lElement.iterator(); ele.hasNext();)
			{
				GaiaMemory gm_ele = ele.next();
				gm_ele.setPlace(gm_ele.place + 1, pos.clone(), true);
			}
		}
	}
	
	private void rotateStyles(GaiaMemory gm, Vertex pos)
	{
		if (gm.place == 1)
		{
			revStyle = true;
			//If we are on the left
			for(Iterator<GaiaMemory> i_style = lStyle.iterator(); i_style.hasNext();)
			{
				GaiaMemory gm_style = i_style.next();
				gm_style.setPlace(gm_style.place - 1, pos.clone(), true);
			}
		}
		else
		{
			revStyle = false;
			//On the right
			for(Iterator<GaiaMemory> ele = lStyle.iterator(); ele.hasNext();)
			{
				GaiaMemory gm_style = ele.next();
				gm_style.setPlace(gm_style.place + 1, pos.clone(), true);
			}
		}
	}
	
	public double Radians(double degrees)
	{
		return degrees * (Math.PI / 180);
	}
	
	public double Degrees(double radians)
	{
		return radians * (180 / Math.PI);
	}
	
	private void fadeMemory(ArrayList<GaiaMemory> memories, boolean isFadingOut)
	{
		for(int iE = 0; iE < memories.size(); iE++)
		{
			GaiaMemory gm = memories.get(iE);
			if (gm.place == 0)
				continue;
			if (isFadingOut)
				gm.fadeOut();
			else
				gm.fadeIn();
		}
	}
	
	private void showIcon(int e, int s)
	{
		Bitmap bE = null;
		Bitmap bS = null;
		
		switch(e)
		{
		case 0:
			bE = BitmapFactory.decodeResource(getResources(), R.drawable.i_cyclone);
			break;
		case 1:
			bE = BitmapFactory.decodeResource(getResources(), R.drawable.i_heat);
			break;
		case 2:
			bE = BitmapFactory.decodeResource(getResources(), R.drawable.i_luna);
			break;
		}
		switch(s)
		{
		case 0:
			bS = BitmapFactory.decodeResource(getResources(), R.drawable.i_joker);
			break;
		case 1:
			bS = BitmapFactory.decodeResource(getResources(), R.drawable.i_metal);
			break;
		case 2:
			bS = BitmapFactory.decodeResource(getResources(), R.drawable.i_trigger);
			break;
		}
		giElement.init(bE);
		giStyle.init(bS);
		
		Vertex gE = Vertex.create(getWidth() / 4, getHeight() / 2);
		//Vertex gE = center.clone();
		//gE.x -= giElement.width * 1;
		giElement.setCenter(gE);
		
		Vertex gS = Vertex.create(getWidth() / 4 * 3, getHeight() / 2);
		//gS.x += giStyle.width * 1;
		giStyle.setCenter(gS);
		
		giElement.fade(getWidth() / 4, getHeight() / 2);
		giStyle.fade(getWidth() / 4, getHeight() / 2);
	}
}
