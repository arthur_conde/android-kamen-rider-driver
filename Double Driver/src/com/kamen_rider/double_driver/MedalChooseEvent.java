package com.kamen_rider.double_driver;

public interface MedalChooseEvent {

	public void onChooseTop();
	public void onChooseMid();
	public void onChooseBot();
}
