package com.kamen_rider.double_driver;

import com.kamen_rider.double_driver.R;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;

public class Henshin extends Activity
	implements playHenshin
{
    /** Called when the activity is first created. */
	private henshinSound sounds;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sounds = new henshinSound(this);
        loadSounds();
        
        //setContentView(R.layout.main);
        setContentView(new DoubleDriver(this, this));
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //attach();
    }
    
    private MediaPlayer mp1;
    private int[] ele_sty;
    
    public void loadSounds()
    {
        sounds.loadSound(R.raw.driver_close);		//id - 0
        sounds.loadSound(R.raw.ins_mem);			//id - 1
        sounds.loadSound(R.raw.henshin_end);		//id - 2
        sounds.loadSound(R.raw.gm_cyclone);			//id - 3
        sounds.loadSound(R.raw.gm_heat);
        sounds.loadSound(R.raw.gm_luna);
        sounds.loadSound(R.raw.gm_joker);			//id - 6
        sounds.loadSound(R.raw.gm_metal);
        sounds.loadSound(R.raw.gm_trigger);
        
        ele_sty = new int[9];
        ele_sty[0] = (R.raw.cyclone_joker);
        ele_sty[1] = (R.raw.cyclone_metal);
        ele_sty[2] = (R.raw.cyclone_trigger);
        ele_sty[3] = (R.raw.heat_joker);
        ele_sty[4] = (R.raw.heat_metal);
        ele_sty[5] = (R.raw.heat_trigger);
        ele_sty[6] = (R.raw.luna_joker);
        ele_sty[7] = (R.raw.luna_metal);
        ele_sty[8] = (R.raw.luna_trigger);
    }

	@Override
	public void playSound(int element, int style) {
		sounds.playSound(element + 3);
		wait(800);
		sounds.playSound(style + 6);
		wait(900);
		if(mp1 != null) 
			mp1.reset();
		mp1 = MediaPlayer.create(this, ele_sty[(3 * element) + style]);
		if (mp1 != null)
		{
			mp1.start();
		}
	}
	
	@Override
	public void playSound(int id) {
		sounds.playSound(id);
	}
	
	public void wait(int ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch (Exception e)
		{
			
		}
	}
}
