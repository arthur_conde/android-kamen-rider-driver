package com.kamen_rider.double_driver;

public interface HenshinEvent {
	public void henshin();
	public void unHenshin();

}
