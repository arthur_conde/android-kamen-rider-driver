package com.kamen_rider.double_driver;

public interface playHenshin {
	/**
	 * Request to play the sound
	 * 
	 * @param element - The element sound to play: cyclone, heat, luna
	 * @param style - The style sound to play: joker, metal, trigger 
	 **/
	public void playSound(int element, int style);
	
	/**
	 * Play sound with id
	 * @param id Id of the sound to play
	 */
	public void playSound(int id);
}
