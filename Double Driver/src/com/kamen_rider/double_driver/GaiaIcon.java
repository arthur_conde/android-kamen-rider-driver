package com.kamen_rider.double_driver;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

public class GaiaIcon {
	private Bitmap anim;
	private Vertex pos;
	private RectF bounds;
	private float alpha = 0;
	private float xs;
	private float ys;
	
	public int height;
	public int width;
	private float FADEIN_TICKS = 15.0f;
	private int WAIT_TICKS = 250;
	private float FADEOUT_TICKS = 20.0f;  
	
	public GaiaIcon()
	{
		setPos(new Vertex());
	}

	/**
	 * @param pos the pos to set
	 */
	public void setPos(Vertex pos) {
		this.pos = pos;
		updateBounds();
	}

	private void updateBounds() {
		this.bounds = new RectF(pos.x, pos.y, pos.x + width, pos.y + height);
	}

	/**
	 * @return the pos
	 */
	public Vertex getPos() {
		return pos;
	}
	
	public Vertex getCenter()
	{
		return Vertex.create(pos.x + width / 2, pos.y + height / 2);
	}
	
	public void setCenter(Vertex pos)
	{
		this.pos = Vertex.create(pos.x - width / 2, pos.y - height / 2);
		updateBounds();
	}
	
	private int fadeMode = 0;
	
	public void fade(int x, int y)
	{
		alpha = 0;
		Ticks = 0;
		fadeMode = 1;
		xs = x / width;
		ys = y / height;
	}
	
	private int Ticks = 0;
	public void update()
	{
		if (fadeMode == 1)
		{
			//Fading in
			alpha += (255 / FADEIN_TICKS);
			if (alpha >= 255)
			{
				fadeMode = 2;
			}
		}
		else if (fadeMode == 2)
		{
			Ticks += 1;
			if (Ticks > WAIT_TICKS)
			{
				fadeMode = 3;
			}
		}
		else if (fadeMode == 3)
		{
			alpha -= (255 / FADEOUT_TICKS);
			if (alpha <= 0)
				fadeMode = 0;
		}
	}
	
	public void draw(Canvas canvas)
	{
		if (this.anim != null)
		{
			Paint m_paint = new Paint();
			m_paint.setAlpha((int) alpha);
			canvas.drawBitmap(this.anim, getMatrix(), m_paint);
		}
	}
	
	private Matrix getMatrix()
	{
		Matrix m = new Matrix();
		m.setTranslate(this.pos.x, this.pos.y);
		if (xs == 0)
			xs = 1.0f;
		if (ys == 0)
			ys = 1.0f;
		Vertex v = this.getCenter();
		m.postScale(xs, ys, v.x, v.y);
		return m;
	}
	
	public void init(Bitmap bitmap)
	{
		this.anim = bitmap;
		height = bitmap.getHeight();
		width = bitmap.getWidth();
	}
	
	public boolean inside(Vertex v)
	{
		RectF my_bounds = this.bounds;;
		getMatrix().mapRect(my_bounds);
		if (v.x < my_bounds.left || v.x > my_bounds.right)
		{
			return false;
		}
		if (v.y < my_bounds.top || v.y > my_bounds.bottom)
		{
			return false;
		}
		return true;
	}
}
